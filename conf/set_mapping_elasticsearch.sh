#!/bin/bash

curl -X PUT -H "Content-Type: application/json" "http://localhost:9200/_template/packets" -d'
{
  "template": "logstash-*",
  "mappings": {
    "doc": {
      "properties": {
        "timestamp": {
          "type": "date"
        },
        "@version": {
	  "type": "integer"
        },
        "host": {
     	  "type": "keyword"
        },
        "@timestamp": {
           "type": "date"
        },
        "protocol": {
	        "type":"keyword"
        },
        "ip_dst": {
           "type": "ip"
        },
        "mac_src": {
           "type": "keyword"
        },
        "ip_src": {
           "type": "ip"
        },
        "mac_dst": {
	   "type":"keyword"

       },
       "pkt_size": {
	   "type":"integer"
       },
       "port_src": {
	   "type":"integer"
       },
       "port_dst": {
           "type":"integer"
       },
       "payload_size": {
           "type":"integer"
       }
      }
     }
  }
}' 

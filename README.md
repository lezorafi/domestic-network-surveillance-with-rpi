# Domestic Network Surveillance

This project is the result of approximately a month’s work on the subject of domestic network security. In this readme, you will find a short introduction about the subject and its importance, followed by a tutorial on how to install this system in you network and how to use it.

## Introduction

With the arrival of affordable Internet-of-Things (IoT)-devices such as smart-TVs, Google Home, Alexa, and even connected light bulbs and cameras, to give a few examples, a domestic network today has an abundance of traffic. As the IoT grows in importance, predictions state that the number of connected things in use will hit 14.2 billion in 2019, and grow to 25 billion by 2021.

This increase in connectivity in people’s homes calls for concern. IoT-devices are typically notorious in terms of security, mostly because the IoT-business is all about being the first to propose a new device on the market. Security takes time, and is therefore not always prioritized. Many IoT-devices have thus shown to be vulnerable to attacks. Another aspect of this dramatic growth of connected devices is the concerns about privacy. The devices themselves are rarely very "smart", as most of the data processing takes place on an externalized server, and the devices simply send data to the server and receives commands. This raises questions about what kind of data is actually being sent to external servers, and what degree of trust we grant to the service provider.

With this in mind, we decided to propose a complete low-cost solution for domestic network surveillance. We used a dedicated device – a Raspberry Pi – for the surveillance, and created a set of configurations to be installed on this device. If you follow the tutorial below, you will find information about how to set up this system, and you will also find instructions on how to install and configure Kibana, a powerful visualization tool, on your computer. This will allow you to monitor you network traffic with ease. In the script folder, you can find scripts that can be used for Time Series analysis of the flow rate and even a Machine Learning algorithm for classification of packets.

## Git structure

The current directory contains configuration and installation scripts

The `cam_analysis` directory contains script we used to analyze the protocol used by our IP Camera.
These scripts are not compatible with generics devices, but if you look at the readme on this directory you will understand the method and perhaps you will even be interested in modifying them for your own purposes.

The `conf` directory contains configuration files for logstash, kibana and elasticsearch. It also contains a script to install python dependencies, which is useful if you want to use our machine learning model.

The `scripts` directory contains useful tools if you want to perform intrusion detection on your domestic network using time series analysis or machine learning. You will learn more if you read the Readme file of this section.

## Installation and configuration of the surveillance system

This section will help you to configure your Raspberry pi as a surveillance system to watch the traffic between your devices and the Internet. To do this we will configure your Pi as an Access Point (or configure an Ethernet access) and use the ELK stack to capture your network flow and perform analysis on it.

### On the Raspberry Pi:

First you need to increase the swap space of your PI.  
To do this just edit /etc/dphys-swapfile and put the value 2048 in the CONF_SWAPSIZE variable.  

Launch the installation_pi.sh script:  
    `sudo installation_pi.sh`


#### Configuring dnsmasq
Edit your /etc/dnsmasq.conf file to match the following configuration:
```
    domain-needed
    bogus-priv
    interface wlan0
    listen-address=10.0.0.1
    bind-interfaces
    dhcp-range=10.0.0.10,10.0.0.100,8h
    dhcp-option=3,10.0.0.1
    dhcp-option=6,10.0.0.1
    log-queries
    log-dhcp
```

#### Configuring hostapd
Edit your /etc/hostapd/hostapd.conf file to match the following configuration:
```    
    interface=wlan0
    driver=nl80211
    ssid=<Your SSID>
    hw_mode=g
    channel=6
    auth_algs=1
    wpa=2
    wpa_passphrase=<Your passphrase>
    wpa_key_mgmt=WPA-PSK
    wpa_pairwise=CCMP
    rsn_pairwise=CCMP
```

#### Configuring elasticsearch:
Edit /etc/elasticsearch/elasticsearch.yml to add the following line:  
`xpack.ml.enabled: false `

Launch the configure_pi.sh script:  
    `sudo configure_pi.sh`

##### Troubleshooting
If the service hostapd is masked:̀ systemctl unmask hostapd`
If ̀ service elasticsearch status` say that config file value is '' edit /etc/default/hostapd and set DAEMON_CONF to "/etc/hostapd/hostapd.conf"
If you got a curl error just run ̀ conf/set_mapping_elasticsearch.sh`

#### Surveillance of an Ethernet network
The configuration below will help you to survey a Wi-Fi network using your Pi as an access point

But if you want to operate on an Ethernet network it is also possible.

You will just need to plug an Ethernet dongle in one of the Pi USB port.

After this you can imagine plug a switch to this dongle and then all your Ethernet equipment will need to pass through the Pi to access Ethernet.

To convert our Wi-Fi surveillance system to an Ethernet one you just need to:  
* Edit /etc/dnsmasq.conf and replace wlan0 by eth1
* Type `service hostapd stop` to disable the Access Point
* Type `ip addr del 10.0.0.1/24 dev wlan0`
* Type `ip addr add 10.0.0.1/24 dev eth1`
* Edit input section of the /etc/logstash/conf.d/logastash_pi.conf and replace wlan0 by eth1

Note:You can also customize your configuration to use both Access Point and Ethernet network together

### On your computer:

Download kibana here: https://www.elastic.co/downloads/kibana
Install it (With deb file: `dpkg -i kibana-X-X-X-x86-64.deb`)

Create a ssh local port redirection between your computer and the raspberry pi to access elasticsearch:  
    `ssh -L 9200:localhost:9200 pi@<ip_addr>`

Launch kibana (on Linux:
    `service kibana start`
)

Access kibana in your browser at the following URL:
    http://localhost:5601

#### Configuring kibana

Before using Kibana to analyze your network, you need to import our views.

To do this perform the following steps:
* Go on the management section of Kibana (on the left menu)
* Click on Saved Objects in the Kibana group
* Click on import at the top right of the page main section.
* Select the kibana.json file in the conf directory of this Git and click on import

You have now new Visualizations and Dashboard to your Kibana interface. But it is configured for the network we used during our experimentations.

Here is a list of update you can do to configure your network:
* Go on the "Network overview" Dashboard, the "active devices" visualization will show you active devices on your network.
* We suggest you to identify your active devices and add them to the devices list on the right.
* To do this go on visualization tab, select "devices list" and update this visualization to match your network.

Now you will see that we have one dashboard by device.
To change the equipment related to a dashboard perform following steps:
* Go on the dashboard you want to edit
* Click on edit button
* Change the filter line and replace existent mac address by the one of the device you want to analyze.
* Click on the save button (you can change dashboard name)
* Now go on the Visualization tab. For each visualization starting with the same acronym than you dashboard at beggining, edit them and replace MAC address by the one of the device you want to analyze.

If you want to add new equipment, just make copy of specifics visualization of one equipment (to do this you can edit a visualization and pick create new visualization when you save) and make copy of one dashboard (you can also make a copy when you save)

Now you can play with kibana and watch what your devices do.

# Extract_payload_type.py
This script read a csv file titled cam_data.csv
For each line of the CSV file it check if the packet is a UDP one or not. 
After this it will classify packets by type using the headers of the UDP payload.
It will also generate a new CSV file by finded type to regroup all packets of this type.
At the end, it will create CSV files who describe the communication between your device and servers mentionned in the method classify traffic.

If you want to use this algorithm you need to:
* format your data to a csv format
* modify all the regex to match with your payload headers
* modify classify traffic method to indicate servers for which trace the communication

# Analyze_payload.py

This script analyze each type of packet individually. 
It will make statistics about packet:
* List of packets size
* List of ports destination
* List of IPs source
* List of IPs dest
* Percentage of unique packet
* Payload size
* Occurence of each bytes of the payload 

If you want to use this script you have to configure payload size (with the array at the beggining of your program) and edit the range in main function to defined type you want to analyze.
import pandas as pd 

extract_len =[[24,59],[36,80],[36,80],[12,5000],[15,5000],[12,5000],[12,5000],[12,35],[24,59],[6,5000],[3,5000],[3,5000],[0,5000],[0,5000],[0,5000],[0,5000],[0,5000],[0,5000]]




def extractMetadata(index):
    ip_dst_list = []

    ip_src_list = []

    port_dst_list = []

    payload_size_list = []

    dataset = pd.read_csv("payload_type"+str(index)+".csv")

    for indexT,row in dataset.iterrows():
        if not row.ip_dst in ip_dst_list:
            ip_dst_list.append(row.ip_dst)
        if not row.ip_src in ip_src_list:
            ip_src_list.append(row.ip_src)
        if not row.port_dst in port_dst_list:
            port_dst_list.append(row.port_dst)
        if not row.payload_size in payload_size_list:
            payload_size_list.append(row.payload_size)

    print("Type "+str(index)+":ip_src, ip_dst, port_dst, payload_size")
    print(ip_src_list,ip_dst_list,port_dst_list,payload_size_list)
    
    dataset = dataset.drop(['@timestamp','ip_src','port_dst','payload_size','protocol','Unnamed: 0'],axis=1)
    return dataset

def getPercentage(x,size):
    return float(x) / size

def extractPayload(dataset,index):
    payloads = dataset['payload'].str[extract_len[index-1][0]:extract_len[index-1][1]]

    knowns_payloads_list = []
    for row in payloads:
        if not row in knowns_payloads_list:
            knowns_payloads_list.append(row)

    knowns_payloads = pd.DataFrame(knowns_payloads_list)
    print(knowns_payloads)
    nonDuplicate = float(knowns_payloads.shape[0])/payloads.shape[0]
    print("Percentage of non-replicate payloads:"+str(nonDuplicate))
    print


    tOccurence = []
    tOccurenceCount = []
    payloadStr = knowns_payloads
    payloadStr = pd.DataFrame(payloadStr)
    for j in range(0,15):
        tOccurence.append([])
        tOccurenceCount.append([])
        payloadSub = payloadStr[0].str[0:3]
        payloadStr = payloadStr[0].str[3:]
        payloadStr = pd.DataFrame(payloadStr)
        # For each payload on known payloads
        for payload in payloadSub:
            # Increment counter if the value is known, otherwise add it to the list
            if payload in tOccurence[j]:
                indexTmp = tOccurence[j].index(payload)
                tOccurenceCount[j][indexTmp] = tOccurenceCount[j][indexTmp] + 1
            else:
                tOccurence[j].append(payload) 
                tOccurenceCount[j].append(1)


    print(len(tOccurence),len(tOccurenceCount))
    occurences = []
    for v in range(0,len(tOccurence)):
        occurences.append(None)
        occurences[v] = pd.DataFrame({
        'value':tOccurence[v],
        'count':tOccurenceCount[v]
        })
        occurences[v].sort_values(by=['count'], inplace=True, ascending=False)
        occurences[v]['percentage'] = occurences[v]['count'].apply(lambda x : getPercentage(x , knowns_payloads.shape[0]))
        print(occurences[v].head(10))

 

def main():
    for i in range(2,3):
        dataset = extractMetadata(i)
        extractPayload(dataset, i)


if __name__ == "__main__":
        main()



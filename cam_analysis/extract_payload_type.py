import pandas as pd 
import re 

dataset = pd.read_csv("cam_data.csv",names=["@timestamp","mac_src","ip_src","port_src","mac_dst","ip_dst","port_dst","protocol","payload","payload_size","pkt_size"])
dataset = dataset.drop(["mac_src","port_src","mac_dst","pkt_size"],axis=1)
toDelete = []
type1 = []
type2 = []
type3 = []
type4 = []
type5 = []
type6 = []
type7 = []
type8 = []
type9 = []
type10 = []
type11 = []
type12 = []
type13 = []
type14 = []
type15 = []
type16 = []
type17 = []
type18 = []
server_80_12_58_212 = []
server_85_195_107_98 = []
server_47_90_208_61 = []
server_47_91_209_73 = []

def classifyTrafic(ipSrc,typeReq,ipDst, portDst):
    if ipSrc == "10.0.0.47" and portDst == 8000:
        if ipDst == "80.12.58.212":
            server_80_12_58_212.append([ipSrc,typeReq,ipDst])
        elif ipDst == "85.195.107.98":
            server_85_195_107_98.append([ipSrc,typeReq,ipDst])
        elif ipDst == "47.90.208.61":
            server_47_90_208_61.append([ipSrc,typeReq,ipDst])
        elif ipDst == "47.91.209.73":
            server_47_91_209_73.append([ipSrc,typeReq,ipDst])
    elif ipSrc == "80.12.58.212" and portDst == 51880:
        server_80_12_58_212.append([ipSrc,typeReq,ipDst])
    elif ipSrc == "85.195.107.98" and portDst == 51880:
        server_85_195_107_98.append([ipSrc,typeReq,ipDst])
    elif ipSrc == "47.90.208.61" and portDst == 51880:
        server_47_90_208_61.append([ipSrc,typeReq,ipDst])
    elif ipSrc == "47.91.209.73" and portDst == 51880:
        server_47_91_209_73.append([ipSrc,typeReq,ipDst])


for index,row in dataset.iterrows():
    if "eth:ethertype:ip:udp:data" == row.protocol: 
        if "10.0.0.255" == row.ip_dst:
            toDelete.append(index)
        if re.match("20:07:04:00:b0:34:19:00*",row.payload):
            toDelete.append(index)
            type1.append(row)
            classifyTrafic(row.ip_src,1,row.ip_dst,row.port_dst)
        if re.match("02:07:ca:a8:00:00:00:00:b0:34:19:00*",row.payload):
            toDelete.append(index)
            type2.append(row)
            classifyTrafic(row.ip_src,2,row.ip_dst,row.port_dst)
        if re.match("01:07:ca:a8:0a:00:00:2f:b0:34:19:00*",row.payload):
            toDelete.append(index)
            type3.append(row)
            classifyTrafic(row.ip_src,3,row.ip_dst,row.port_dst)
        if re.match("11:03:50:00*",row.payload):
            toDelete.append(index)
            type4.append(row)
            classifyTrafic(row.ip_src,4,row.ip_dst,row.port_dst)
        if re.match("00:01:00:00*",row.payload):
            toDelete.append(index)
            type5.append(row)
            classifyTrafic(row.ip_src,5,row.ip_dst,row.port_dst)
        if re.match("09:01:00:00*",row.payload):
            toDelete.append(index)
            type6.append(row)
            classifyTrafic(row.ip_src,6,row.ip_dst,row.port_dst)
        if re.match("0c:14:07:00*",row.payload):
            toDelete.append(index)
            type7.append(row)
            classifyTrafic(row.ip_src,7,row.ip_dst,row.port_dst)
        if re.match("0d:01*",row.payload):
            toDelete.append(index)
            type8.append(row)
            classifyTrafic(row.ip_src,8,row.ip_dst,row.port_dst)
        if re.match("20:07*",row.payload):
            toDelete.append(index)
            type9.append(row)
            classifyTrafic(row.ip_src,9,row.ip_dst,row.port_dst)
        if re.match("21:07*",row.payload):
            toDelete.append(index)
            type10.append(row)
            classifyTrafic(row.ip_src,10,row.ip_dst,row.port_dst)
        if re.match("10:07*",row.payload):
            toDelete.append(index)
            type11.append(row)
            classifyTrafic(row.ip_src,11,row.ip_dst,row.port_dst)
        if re.match("11:07*",row.payload):
            toDelete.append(index)
            type12.append(row)
            classifyTrafic(row.ip_src,12,row.ip_dst,row.port_dst)
        if re.match("^56:*",row.payload):
            toDelete.append(index)
            type13.append(row)
            classifyTrafic(row.ip_src,13,row.ip_dst,row.port_dst)
        if re.match("^54:*",row.payload):
            toDelete.append(index)
            type14.append(row)
            classifyTrafic(row.ip_src,14,row.ip_dst,row.port_dst)
        if re.match("^06:*",row.payload):
            toDelete.append(index)
            type15.append(row)
            classifyTrafic(row.ip_src,15,row.ip_dst,row.port_dst)
        if re.match("^50:*",row.payload):
            toDelete.append(index)
            type16.append(row)
            classifyTrafic(row.ip_src,16,row.ip_dst,row.port_dst)
        if re.match("^53:*",row.payload):
            toDelete.append(index)
            type17.append(row)
            classifyTrafic(row.ip_src,17,row.ip_dst,row.port_dst)
        if re.match("^55:*",row.payload):
            toDelete.append(index)
            type18.append(row)
            classifyTrafic(row.ip_src,18,row.ip_dst,row.port_dst)
    else:
        toDelete.append(index)
    

dataset = dataset.drop(dataset.index[toDelete])
type1 = pd.DataFrame(type1)
type2 = pd.DataFrame(type2)
type3 = pd.DataFrame(type3)
type4 = pd.DataFrame(type4)
type5 = pd.DataFrame(type5)
type6 = pd.DataFrame(type6)
type7 = pd.DataFrame(type7)
type8 = pd.DataFrame(type8)
type9 = pd.DataFrame(type9)
type10 = pd.DataFrame(type10)
type11 = pd.DataFrame(type11)
type12 = pd.DataFrame(type12)
type13 = pd.DataFrame(type13)
type14 = pd.DataFrame(type14)
type15 = pd.DataFrame(type15)
type16 = pd.DataFrame(type16)
type17 = pd.DataFrame(type17)
type18 = pd.DataFrame(type18)

type1.to_csv("payload_type1.csv")
type2.to_csv("payload_type2.csv")
type3.to_csv("payload_type3.csv")
type4.to_csv("payload_type4.csv")
type5.to_csv("payload_type5.csv")
type6.to_csv("payload_type6.csv")
type7.to_csv("payload_type7.csv")
type8.to_csv("payload_type8.csv")
type9.to_csv("payload_type9.csv")
type10.to_csv("payload_type10.csv")
type11.to_csv("payload_type11.csv")
type12.to_csv("payload_type12.csv")
type13.to_csv("payload_type13.csv")
type14.to_csv("payload_type14.csv")
type15.to_csv("payload_type15.csv")
type16.to_csv("payload_type16.csv")
type17.to_csv("payload_type17.csv")
type18.to_csv("payload_type18.csv")

    

print("Dataset:")
dataset2 = dataset.drop(["@timestamp","protocol"],axis=1)
print(dataset2)

server_80_12_58_212 = pd.DataFrame(server_80_12_58_212)
server_85_195_107_98 = pd.DataFrame(server_85_195_107_98)
server_47_90_208_61 = pd.DataFrame(server_47_90_208_61)
server_47_91_209_73 = pd.DataFrame(server_47_91_209_73)

server_80_12_58_212.to_csv("server_80_12_58_212.csv")
server_85_195_107_98.to_csv("server_85_195_107_98.csv")
server_47_90_208_61.to_csv("server_47_90_208_61.csv")
server_47_91_209_73.to_csv("server_47_91_209_73.csv")
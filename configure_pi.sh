# Configuring AP
# Set ip on wlan0 interface
ip addr add 10.0.0.1/24 dev wlan0
# Enable ip forwarding
echo 1 > /proc/sys/net/ipv4/ip_forward

# Configuring ip masquerade
iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE 

# Load config files
cp ./conf/logstash_pi.conf /etc/logstash/conf.d/logstash_pi.conf

# Modify JVM options
sed -i -e 's/-Xms1g/-Xms500m/g' /etc/elasticsearch/jvm.options 
sed -i -e 's/-Xmx1g/-Xmx500m/g' /etc/elasticsearch/jvm.options 
sed -i -e 's/-Xms1g/-Xms500m/g' /etc/logstash/jvm.options 
sed -i -e 's/-Xmx1g/-Xmx500m/g' /etc/logstash/jvm.options 

# Allow logstash user to capture traffic with tshark
chmod +x /usr/bin/dumpcap

# Launching services
service ssh start
service hostapd start
service dnsmasq start
service elasticsearch start
service logstash start

conf/set_mapping_elasticsearch.sh

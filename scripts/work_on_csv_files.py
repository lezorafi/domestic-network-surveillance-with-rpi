#from elasticsearch import Elasticsearch
import pandas
from pandas import Series
import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import leastsq
import sys

#%% Import csv file and create DataFrame

def import_csv_to_dataframe(csv_file_name):
    df = pandas.read_csv(csv_file_name, index_col=False)
    df["hour"] = Series("test", index=df.index)
    for index, rows in df.iterrows():
        val = index % 24
        df.at[index, 'hour'] = val
    df = df.set_index("date_hour")

    return df

def plot_flow_and_rolling_mean(df):
    df['rolling_mean'] = df['nb_req'].rolling(3).mean()

    N = len(df)
    t = np.linspace(0, N-1, num=N)

    fig, ax1 = plt.subplots()
#    x = df.index # not used as x-axis as it is a string
    y1 = df['nb_req']
    y2 = df['rolling_mean']

    ax1.plot(t, y1, 'g-', label='Actual packets')
    plt.xlabel('Hours since beginning')
    ax1.legend("Nb req")
    ax1.plot(t, y2, 'b-', label='Rolling mean on 3h')


    plt.title('First prediction attempt: rolling mean')
    plt.ylabel('Debit (requests per hour)')

    plt.legend()



    plt.show()

def scatter_plot_flow_per_hour_of_day(liste_heures):

    hours = xrange(24)
    for xe, ye in zip(hours, liste_heures):
        plt.scatter([xe]*len(ye), ye)

    plt.xlabel('Hour of the day')
    plt.ylabel("Number of requests per hour")
    plt.title('Requests grouped by time of day')
    plt.xticks(hours)
    plt.show()

def create_list_of_flow_per_hour(df2):
    liste_heures = []

    for hour in xrange(24):
        liste_heures.append([])
        for index, rows in df2.iterrows():
            if (index%24 == hour):
                liste_heures[hour].append(rows["nb_req"])

    return liste_heures

def plot_errorbar(liste_heures, mean, std):

    hours = xrange(24)
    plt.errorbar(hours, mean, yerr=std)
    plt.title("Mean and sample standard deviation of flow each hour of the day")
    plt.xlabel("Hour of the day")
    plt.ylabel("Number of requests per hour")
    plt.xticks(hours)
    plt.show()

def mean_per_hour(liste_heures):
    mean = []
    for hour in liste_heures:
        mean.append(np.mean(hour))
    return mean

def std_per_hour(liste_heures):
    sample_std_dev =[] #PS: SAMPLE standard deviation (we do not take every value
                        # into consideration)
    for hour in liste_heures:
        sample_std_dev.append(np.std(hour, ddof=1))

    return sample_std_dev

def create_sinewave(df, mean, N, t):

    # first estimation
    guess_mean = np.mean(mean)
    guess_std = 3*np.std(df['nb_req'])/(2**0.5)
    guess_phase = np.pi
    guess_freq = float(1.0 /24)
    guess_amp = 1

    # Define the function to optimize, in this case, we want to minimize the difference
    # between the actual data and our "guessed" parameters
    optimize_func = lambda x: x[0]*np.sin(x[1]*t+x[2]) + x[3] - df['nb_req']
    est_amp, est_freq, est_phase, est_mean = leastsq(optimize_func, [guess_amp, guess_freq, guess_phase, guess_mean])[0]
    data_fit = est_amp*np.sin(est_freq*t+est_phase) + est_mean

    data_first_guess = guess_std*np.sin(2*np.pi*guess_freq*t+guess_phase) + guess_mean
    plt.plot(t, df['nb_req'], '.', label='Nb packets')
    plt.plot(t, data_first_guess, label='first guess')
    plt.plot(t, data_fit, label='estimate')
    plt.legend()
    plt.title('Simple EWMA')
    plt.show()


def plot_fft(df):
    rfft = np.abs(np.fft.rfft(df['nb_req']))
    plt.plot(rfft)
    plt.title("Fourier Transform of the flow")
    plt.xlabel("Frequency (hours)")
    plt.show()

def simple_ewma(df, t, span, stdlimit):

    df_test = pandas.DataFrame(df['nb_req'])
    df_test_ewma_pred = df_test.ewm(span=span).mean()
    df_test_ewmastd = df_test.ewm(span=span).std()
    df_test_nreq_stds = ((df_test -  df_test_ewma_pred) / df_test_ewmastd)
    df_outlier = df_test_nreq_stds.abs() > 1 #to be determined

    fig, ax1 = plt.subplots()
    x = t
    y1 = df_test['nb_req']
    y2 = df_test_ewma_pred['nb_req']
    y3 = df_outlier['nb_req']



    ax1.legend("Actual flow")


    ax1.plot(x, y1, 'g-', label='Actual packets')
    ax1.plot(x, y2, 'b-', label='Prediction')
    plt.legend()
    plt.ylabel('Flow (packets/hour)')
    plt.xlabel('Hours since beginning')

    ax2 = ax1.twinx()
    ax2.legend("Prediction (normal EWMA)")

#    ax1.set_label_position("left")
    ax2.plot(x, y3, 'r+')
    plt.ylabel('Alert (boolean)')
    plt.title('Simple EWMA')



    plt.show()

# computes a "stacked"/periodic ewma where the same hour the preceding days
# is what determines the prediction
def stacked_ewma(liste_heures, t, span, df):

    dflh = pandas.DataFrame(liste_heures)
    dflh2 = dflh.transpose()

    dflh_test_ewma_pred = dflh2.ewm(span=span).mean()
    dflh_test_ewmastd = dflh2.ewm(span=span).std()

    dflh_test_ewma_pred = dflh_test_ewma_pred.transpose()
    dflh_test_ewmastd = dflh_test_ewmastd.transpose()

    dflh_pred_liste = []
    for tic, data in dflh_test_ewma_pred.iteritems():
        dflh_pred_liste.append(dflh_test_ewma_pred.iloc[:,tic])

    dflh_pred_series = pandas.concat(dflh_pred_liste)

    dflh_std_liste = []
    for tic, data in dflh_test_ewma_pred.iteritems():
        dflh_std_liste.append(dflh_test_ewmastd.iloc[:,tic])

    dflh_std_series = pandas.concat(dflh_std_liste)


    dflh_emwastd = dflh_std_series.tolist()
    dflh_pred = dflh_pred_series.tolist()
    df_aux = df['nb_req'].tolist()

    dflh_debit_std = []
    for hour in range(0, len(df_aux)):
        dflh_debit_std.append([])
        if dflh_emwastd[hour] > 0:
            dflh_debit_std[hour] = (df_aux[hour] - dflh_pred[hour])/ dflh_emwastd[hour]
        else:
            dflh_debit_std[hour] = (df_aux[hour] - dflh_pred[hour])/ 1


    dflh_debit_std = pandas.Series (dflh_debit_std)

    df_outlier_stacked = dflh_debit_std.abs() > 1 #to be determined


    fig, ax1 = plt.subplots()

    y1 = df['nb_req']
    y2 = dflh_pred[0:len(df)]

    ax1.plot(t, y1, 'g-', label='Actual packets')
    ax1.plot(t, y2, 'b-', label='Prediction')
#    ax1.set_label_position("left")
    plt.legend()
    plt.xlabel('Hours since beginning')
    plt.ylabel('Flow (packets/hour)')

    ax2 = ax1.twinx()
    ax2.plot(t, df_outlier_stacked, 'r+')
    plt.ylabel('Alert (boolean)')

    plt.title('Stacked EWMA')
    plt.show()

def main():
    print ("Analyzing")
    file_name = sys.argv[1]
    df = import_csv_to_dataframe(file_name)

    plot_flow_and_rolling_mean(df)

    df2 = df.set_index("hour")

    liste_heures = create_list_of_flow_per_hour(df2)

    scatter_plot_flow_per_hour_of_day(liste_heures)

    mean = mean_per_hour(liste_heures)
    std = std_per_hour(liste_heures)

    plot_errorbar(liste_heures, mean, std)


    #%% Create sinewave to fit
    N = len(df)
    t = np.linspace(0, N-1, num=N)

    create_sinewave(df, mean, N, t)

    #very bad fit

    #%% Determine if there is a daily pattern
    plot_fft(df)
    # conclusion : not really...

    #%% simple ewma

    simple_ewma(df, t, span=6, stdlimit=5)

    #%% stacked ewma

    stacked_ewma(liste_heures, t, df=df, span=5)

if __name__== "__main__":
    main()

from pandasticsearch import DataFrame
from elasticsearch import Elasticsearch
import pandas
from pandas import to_datetime, Series, Timedelta
from collections import Counter
from natsort import natsorted
import matplotlib.pyplot as plt

addr_mac_cam1 = "14:6b:9c:9a:0d:a3"
addr_mac_cam2 = "4c:b0:08:60:10:1e"

addr_mac_tv = "f4:7b:5e:aa:c6:b9"

addr_mac_light = "ec:b5:fa:02:9d:64"

def fetch_dataframe(start_date, end_date, conditions, df):
    frames = []
    for day in xrange(start_date,end_date,1):
        if day != 17 and day != 18 and day != 19:
            for hour in xrange(0,24,1):
                start = '2019-02-%02dT%02d:00:00' % (day,hour)
                end = '2019-02-%02dT%02d:59:59'% (day,hour)

                for req in xrange(0,len(conditions),1):
                    df = df.filter((df["@timestamp"]>= start)
                                    & (df["@timestamp"]<= end)
                                    & conditions[req])

                    df = df.select('@timestamp').limit(10000)

                    dfp = df.to_pandas()
                    if dfp is not None:
                        dfp["@timestamp"] = pandas.to_datetime(dfp["@timestamp"])
                        dfp = dfp.set_index("@timestamp")
                        print(dfp)
                        frames.append(dfp)

    result = pandas.concat(frames)

    return result

def format_dataframe(df):

    df = df.drop("_id", axis=1)
    df = df.drop("_index", axis=1)
    df = df.drop("_type", axis=1)
    df = df.drop("_score", axis=1)

    df["date_hour"] = Series("test", index=df.index)
    df["date"] = Series("test", index=df.index)
    df["hour"] = Series("test", index=df.index)

    for index, rows in df.iterrows():
        # Add column that allows to identify date and hour
        rows["date_hour"] = str(index.month) + '-' + str(index.day) + '-' + str(index.hour)
        # (Add two extra columns for clarity)
        rows["date"] = str(index.year) + '-' + str(index.month) + '-' + str(index.day)
        rows["hour"] = str(index.hour)

    return df

# returns dataframe containing the "date_hour"-column
# with the associated flow
def compute_flow(df):
    #Count number of packets per hour for each day
    counter = Counter(df["date_hour"])
    s = Series (df.date_hour)
    vc = s.value_counts()
    # Sort chronologically (MM-DD-HH)
    vc2 = natsorted(vc.index)

    #create dataframe with the sorted date_hour as index
    df2 = pandas.DataFrame(index=vc2)
    #Create column with the associated flow
    df2["nb_req"] = Series("0", index=df2.index)
    for index, rows in df2.iterrows():
        rows["nb_req"] = counter[index]
    print(df2)

    df2.plot(kind='line')

    plt.show()
    return df2

def main():
    print("Fetching data from elasticsearch!")
    es = Elasticsearch()

    es.indices.refresh(index="logstash*")
    df = DataFrame.from_es(url='http://localhost:9200', index='logstash*', compat=5)

    cond_camera = [df["mac_src"] == addr_mac_cam1,
                  df["mac_src"] == addr_mac_cam2,
                  df["mac_dst"] == addr_mac_cam1,
                  df["mac_dst"] == addr_mac_cam2]

    cond_tv = [df["mac_src"] == addr_mac_tv,
                 df["mac_dst"] == addr_mac_tv]

    cond_light = [df["mac_src"] == addr_mac_light,
                 df["mac_dst"] == addr_mac_light]

    result = fetch_dataframe(26, 27, cond_camera, df)

    print(result)

    result = format_dataframe(result)
    #result.to_csv('timestamps_attack_25_26.csv', encoding='utf-8')

    flow = compute_flow(result)
    flow.to_csv('attack_camera.csv', encoding='utf-8')

if __name__== "__main__":
    main()

# Anomaly Detection
This readme explains how to use the scripts we created for anomaly detection in a domestic network.

## Time Series Analysis for Building a Threat Model

This section explains how to conduct a time series analysis on a set of data in order to do a threat detection. You need to use two (or three) scripts. The first script solicits the Elasticsearch database and acquires the Time Series, and saves it as a csv-file. The second script takes the csv-file as input and makes a prediction for each hour, and analyzes whether or not the actual flow deviates too much from the prediction. You might need to use a third script (we will get back to this).

__Do the following:__
1.	Edit the script `fetch_data_from_several_days.py` to set the days you want to fetch data from. In the main function, you can set the dates on line 103. By default, the month is February, and the dates are set to Feb 26 – Feb 27. This was when we conducted an attack on our camera. You can therefore see on line 111 that we chose to name the csv-file `attack_camera.csv`. Modify this file-name as you wish. If you want to change the month as well, you need to modify the function `fetch_dataframe` on the lines 21 and 22.
2. When you have run the script with the wanted dates and file-name, you might want to check if the resulting csv-file has data for each and every hour. If the device emits permanently, like the IP camera, it has data for every hour, but if it stops when turned off, like the smart-tv, you will need to parse the file. We created a script for this, as it might be a tedious task to do manually. Just call the script `parse-csv.py` with the csv-filename as the argument. This is the third script we mentioned earlier. It will produce a parsed file called `new.csv`, ready for analysis.  
3.	Edit the main function in the script called `work_on_csv_files.py` to match what you want to do. By default, it does everything we did during our prototyping, but you might want to comment out line 230 and 245-258 if you want to jump straight to the “periodic” ewma. This last prediction method is what we found to be the most efficient.
4.	Run `work_on_csv_files.py` with the csv-file you want to analyze as the parameter. Enjoy.


## Threat Detection using Machine Learning Algorithms
We used two different algorithms to create a machine learning model of our network traffic. They are described below.

### one_class_svm.py

This tool will attempt to detect intrusion on your network using the One Class SVM algorithm.

You should read the beggining of the script to configure main parameters as elasticsearch URL, training phase,...

__The behavior of this script is the following:__
1. The algorithm will retrieve data from elasticsearch for the period defined by the paramaters at the beggining of the script
1. Retrieved data will be computed and scaled to be a numerical value between 0 and 1
1. Input data are gived to the One Class SVM algorithm
1. If you are in debug mode some data from specific period (corresponding to attack during our project) will be treated
1. A python server will start and you could pass data to the model using test route with a POST request.

You can uncomment the lines related to this http server in the logstash config file if you want to use this server to perform real time analysis. A visualization also exists in Kibana to monitor the proportion of illegitimate and legitimate traffic.


### random_forests.py

This tool will attempt to detect intrusion on your network using the random forests algorithm.

You should read the beggining of the script to configure main parameters as elasticsearch URL, training phase,...

__The behavior of this script is the following:__
1. The algorithm will retrieve data from elasticsearch for the period defined by the paramaters at the beggining of the script
1. Data will be tagged as legitimate or illegitimate (you need to modify the tagData function to configure what you consider as an illegitimate packet)
1. Retrieved data will be computed and scaled to be a numerical value between 0 and 1
1. Input data are gived to the random forests algorithm
1. If you are in debug mode some data from specific period (corresponding to attack during our project) will be treated
1. A python server will start and you could pass data to the model using test route with a POST request.

You can uncomment the lines related to this http server in the logstash config file if you want to use this server to perform real time analysis. A visualization also exist in Kibana to watch the proportion of illegitimate and legitimate traffic.

import pandas
from pandas import Series
import sys
import os

def import_csv_to_dataframe(csv_file_name):
    df = pandas.read_csv(csv_file_name, index_col=False)
    df = df.set_index("date_hour")
    
    
    return df


def remove_data_from_before_midnight(df):
        
    #delete everything before first measurement at midnight
    wanted_start_hour = '0'
    while True:
        first_value = df.iloc[0]
        start_date, partitioner, start_hour = first_value.name.rpartition('-')
        if start_hour == wanted_start_hour:
            break
        df.drop(df.head(1).index, inplace=True)

    return df


def insert_hours_with_zero_when_no_data(df):
    
    expected_hour = 0
    previous_index = str(df.head(1).index)
    df_new = df
    
    date,partitioner,hour= previous_index.rpartition('-')
    month, partitioner, day = date.rpartition('-')
    expected_day = int(day)
    
    for index, rows in df.iterrows():
        date,partitioner,hour= index.rpartition('-')
        month, partitioner, day = date.rpartition('-')
        incr_day = False
        # fill with zeroes until we reach the next day with recorded traffic
        while(int(day) != expected_day):
            
            if int(expected_hour) != 0 or incr_day == True:
                incr_day = False
                print("Missing data from day", expected_day)
                print("Read hour: ", int(hour))
                print("Missing hour: ", expected_hour)
                date_hour_to_insert = month + '-' + str(expected_day) + '-' + str(expected_hour)           
                df_start = df_new.loc[:previous_index]
                df_end = df_new.loc[index:]
               
                print("Insertion of:")
                line = pandas.DataFrame({"nb_req": 0}, index=[date_hour_to_insert])
                print(line)
                liste = [df_start, line, df_end]
                df_new = pandas.concat(liste)
                previous_index = date_hour_to_insert
                expected_hour += 1
                expected_hour = expected_hour%24
                if expected_hour == 0:
                    expected_day = (expected_day + 1)%28
                    incr_day = True
                    
        # fill with zeroes until we reach the next hour with recorded traffic 
        # of the current day 
        while int(hour) != expected_hour:
            print("Missing data from day", expected_day)
            print("Read hour: ", int(hour))
            print("Missing hour: ", expected_hour)
            date_hour_to_insert = month + '-' + str(expected_day) + '-' + str(expected_hour)           
            df_start = df_new.loc[:previous_index]
            df_end = df_new.loc[index:]
           
            print("Insertion of:")
            line = pandas.DataFrame({"nb_req": 0}, index=[date_hour_to_insert])
            print(line)
            liste = [df_start, line, df_end]
            df_new = pandas.concat(liste)
            previous_index = date_hour_to_insert
            expected_hour += 1
            expected_hour = expected_hour%24
    
        expected_hour += 1
        expected_hour = expected_hour%24
        
        if expected_hour == 0:
            expected_day = (expected_day + 1)%28
        previous_index = index
        
    return df_new

def main():
    print ("Parsing...")
    file_name = sys.argv[1]
    df = import_csv_to_dataframe(file_name)
    df = remove_data_from_before_midnight(df)
    df = insert_hours_with_zero_when_no_data(df)
    print("parsed dataframe:")
    
    #add hour for simplicity
    df["hour"] = Series("test", index=df.index)
    hour = 0
    for index, rows in df.iterrows():
        val = hour % 24
        df.at[index, 'hour'] = val
        hour += 1
        
    df.to_csv('new.csv', encoding='utf-8')
    
    #fix the problem with missing column names
    with open("new.csv",'r') as f:
        with open("parsed.csv",'w') as f1:
            f.next() # skip existing header line
            f1.write("date_hour,nb_req,hour\n") #write new header
            for line in f:
                f1.write(line)

    if os.path.exists("new.csv"):
        os.remove("new.csv")
        
    print("Parsed. Saved in file named parsed.csv")

if __name__== "__main__":
    main()

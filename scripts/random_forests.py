# Create a DataFrame object
from pandasticsearch import DataFrame
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import OneClassSVM
from sklearn.tree import export_graphviz
import matplotlib.pyplot as plt
import re
from flask import Flask, request
from flask_restful import Resource, Api
from flask_jsonpify import jsonify
from datetime import datetime, timedelta

app = Flask(__name__)
api = Api(app)


# Configuration AREA 
DEBUG = False

# Elasticsearch URL
ELASTIC_URL = 'http://localhost:9200'

# Elasticsearch index
ELASTIC_INDEX = 'logstash*'

# Training data date range
# Year, Month, Day, Hours, Minutes, Seconds
startDate = [2019,2,26,12,0,0]
# Number of hour you want to use after start date
nbHours = 6

# Write MAC Adresses of your Network devices
network_devices = [["14:6b:9c:9a:0d:a3","4c:b0:08:60:10:1e"],["ec:b5:fa:02:9d:64"],["f4:7b:5e:aa:c6:b9"],["b8:27:eb:89:fc:ff ","00:e0:4c:68:09:47"]]

# Give current IP that you see on your network and that you consider as trusted
knowns_ip = []
knowns_ip.append("85.195.107.98")
knowns_ip.append("47.90.208.61")
knowns_ip.append("31.4.183.115")
knowns_ip.append("176.83.32.54")
knowns_ip.append("47.91.209.73")
knowns_ip.append("185.69.144.15")
knowns_ip.append("80.12.58.196")
knowns_ip.append("95.127.170.32")
knowns_ip.append("146.71.125.154")
knowns_ip.append("83.59.6.113")


# END OF CONFIGURATION AREA 

forest = ''
scaler = ''

def isLocal( addr ):
        "Determine if a IP is from the local network or not"
        if addr != addr:
                return 1
        if re.match("^10*",str(addr)) or re.match("^192*",str(addr)):
                return 1
        else:
                return 0
            
def isARP( protocol ):
   if "arp" in protocol:
       return 1
   else: 
       return 0

def isUDP( protocol ):
   if "udp"in protocol:
       return 1
   else: 
       return 0
   
def isTCP( protocol ):
   if "tcp" in protocol:
       return 1
   else: 
       return 0
   
def isDNS( protocol ):
   if "dns" in protocol:
       return 1
   else: 
       return 0




def getData():
        "Retrieve data from elasticsearch"
        df = DataFrame.from_es(url= ELASTIC_URL, index=ELASTIC_INDEX, compat=5)
        dataDate = datetime(startDate[0],startDate[1],startDate[2],startDate[3],startDate[4],startDate[5])
        dataDate1 = dataDate + timedelta(minutes=10)
        final = df.filter((df.timestamp > str(dataDate.isoformat()))&(df.timestamp < str(dataDate1.isoformat()))).select('@timestamp','mac_src','ip_src','mac_dst','ip_dst','port_dst','protocol','payload','payload_size','pkt_size').limit(10000)
        final = final.to_pandas()
        #final['row_count'] = final.shape[0]
        for i in range(0,nbHours*6):
            dataDate = dataDate + timedelta(minutes=10)
            dataDate1 = dataDate1 + timedelta(minutes=10)
            row = df.filter((df.timestamp > dataDate.isoformat())&(df.timestamp < dataDate1.isoformat())).select('@timestamp','mac_src','ip_src','mac_dst','ip_dst','port_dst','protocol','payload','payload_size','pkt_size').limit(10000)
            row = row.to_pandas()
            #row['row_count'] = row.shape[0]
            #print(row)
            final = pd.concat([final,row],ignore_index=True)
        
        final = final.drop(["_id","_index","_score","_type"],axis=1)
        return final

def getDataWhenAttack():
        df = DataFrame.from_es(url=ELASTIC_URL, index=ELASTIC_INDEX, compat=5)
        row = df.filter((df.timestamp > "2019-02-26T15:15:00")&(df.timestamp < "2019-02-26T15:45:00")).select('@timestamp','mac_src','ip_src','mac_dst','ip_dst','port_dst','protocol','payload','payload_size','pkt_size').limit(10000)
        row = row.to_pandas()
        row = row.drop(["_id","_index","_score","_type"],axis=1)
        return row

def getRowCount( timestamp ):
    df = DataFrame.from_es(url= ELASTIC_URL, index=ELASTIC_INDEX, compat=5)
    timestampStart = timestamp - timedelta(minutes=10)
    result = df.filter((df.timestamp > str(timestampStart.isoformat()))&(df.timestamp < str(timestamp.isoformat()))).agg(df.timestamp.distinct_count).collect()
    return result[0]['cardinality(timestamp)']

def tagData( dataset ):
        "Tag training data from elasticsearch as true"
        datasetSize = len(dataset.index)
        target = []
        for i in range(0,datasetSize):
                if dataset.loc[i].ip_src == "192.168.1.20" or dataset.loc[i].ip_dst == "192.168.1.20":
                    target.append('false')
                elif dataset.loc[i].ip_src == "10.0.0.84" or dataset.loc[i].ip_dst == "10.0.0.84":
                    target.append('false')
                elif dataset.loc[i].ip_src == "110.0.0.85" or dataset.loc[i].ip_dst == "10.0.0.85":
                    target.append('false')
                else:
                    target.append('true')
        return target

def computeData( dataset ):
        "Transform original dataset to make it more pertinent for machine learning analysis"
        dataset['input_pkt'] = 'input_pkt'
        dataset['known_ip'] = 'known_ip'
        dataset['local_ip'] = 'local_ip'
        dataset['is_arp'] = 'is_arp'
        dataset['is_udp'] = 'is_udp'
        dataset['is_tcp'] = 'is_tcp'
        dataset['is_dns'] = 'is_dns'
        dataset['no_payload'] = 'no_payload'
        #dataset['hours'] = 'hours'
        #dataset['day_of_week'] = 'day_of_week'
        for nbDevice in range(0,len(network_devices)):
            dataset['device_'+str(nbDevice)] = 'device_'+str(nbDevice)
        dataset['other_devices'] = 'other_devices'

        for index, row in dataset.iterrows():
            #if row['row_count'] == -1 :
            #    dataset.at[index, 'row_count'] = getRowCount(pd.to_datetime(row['@timestamp']))
            #dataset.at[index,'hours'] = pd.to_datetime(row['@timestamp']).hour
            #dataset.at[index,'day_of_week'] = pd.to_datetime(row['@timestamp']).dayofweek
            if 'port_dst' in str(row.port_dst):
                dataset.at[index,'port_dst'] = 0
            source = None
            destination = None
            present = False
            for device in range(0,len(network_devices)):
                if row.mac_src in network_devices[device]:
                    dataset.at[index,'device_'+str(device)] = 1
                    source = row.mac_src
                    present = True
                elif row.mac_dst in network_devices[device]:
                     dataset.at[index,'device_'+str(device)] = 1
                     destination = row.mac_dst
                     present = True
                     
            if not present:
                dataset.at[index,'other_devices'] = 1
            else:
                dataset.at[index,'other_devices'] = 0
            
            if row.port_dst != row.port_dst:
                dataset.at[index,'port_dst'] = 0
            if source != None:
                dataset.at[index,'input_pkt'] = 0
                if row.ip_dst in knowns_ip:
                    dataset.at[index,'known_ip'] = 1
                else: 
                    dataset.at[index,'known_ip'] = 0

                dataset.at[index,'local_ip'] = isLocal(row.ip_dst)
            elif destination != None:
                dataset.at[index,'input_pkt'] = 1
                if row.ip_src in knowns_ip:
                    dataset.at[index,'known_ip'] = 1
                else:
                    dataset.at[index,'known_ip'] = 0
            
                dataset.at[index,'local_ip'] = isLocal(row.ip_src)
            else: #Device is unknown
                dataset.at[index,'input_pkt'] = 0.5
                if row.ip_src in knowns_ip:
                    dataset.at[index,'known_ip'] = 1
                else:
                    dataset.at[index,'known_ip'] = 0
            
                dataset.at[index,'local_ip'] = isLocal(row.ip_src)
                
            dataset.at[index,'is_arp'] = isARP(row.protocol)   
            dataset.at[index,'is_udp'] = isUDP(row.protocol)
            dataset.at[index,'is_tcp'] = isTCP(row.protocol)
            dataset.at[index,'is_dns'] = isDNS(row.protocol)
            for nbDevice in range(0,len(network_devices)):
                if re.match("device",str(row['device_'+str(nbDevice)])) :
                    dataset.at[index,'device_'+str(nbDevice)] = 0
                    
            if row.payload != row.payload:
                 dataset.at[index,'no_payload'] = 1
            else:
                dataset.at[index,'no_payload'] = 0

            
            
        dataset = dataset.drop(['@timestamp','mac_src','ip_src','mac_dst','ip_dst','protocol','payload','payload_size'],axis=1)
        return dataset


def scaleDataset( X_train,X_test ):
        "Dataset Scaling"
        global scaler
        # Dataset Scaling
        scaler = MinMaxScaler()
        scaler.fit(X_train)

        X_train_scaled = scaler.transform(X_train)
        if DEBUG:
            print("transformed shape: {}".format(X_train_scaled.shape))
            print("per-feature minimum before scaling:\n {}".format(X_train.min(axis=0)))
            print("per-feature maximum before scaling:\n {}".format(X_train.max(axis=0)))
            print("per-feature minimum after scaling:\n {}".format(X_train_scaled.min(axis=0)))
            print("per-feature maximum after scaling:\n {}".format(X_train_scaled.max(axis=0)))

        X_test_scaled = scaler.transform(X_test)
        if DEBUG:
            print("transformed shape: {}".format(X_train_scaled.shape))
            print("per-feature minimum before scaling:\n {}".format(X_test.min(axis=0)))
            print("per-feature maximum before scaling:\n {}".format(X_test.max(axis=0)))
            print("per-feature minimum after scaling:\n {}".format(X_test_scaled.min(axis=0)))
            print("per-feature maximum after scaling:\n {}".format(X_test_scaled.max(axis=0)))

        return X_train_scaled, X_test_scaled




def classify(X_train_scaled,y_train,X_test_scaled,y_test):
        global forest
        # Use random forest classifier
        forest = RandomForestClassifier(n_estimators = 100, random_state = 2, max_depth = 3)
        forest.fit(X_train_scaled,y_train)

        print("Accuracy on training set: {:.3f}".format(forest.score(X_train_scaled, y_train)))
        print("Accuracy on test set: {:.3f}".format(forest.score(X_test_scaled, y_test)))

        featureName = ["pkt_size",'port_dst'#,'row_count'
                        ,'input_pkt','known_ip','local_ip'
                        ,'is_arp','is_udp','is_tcp'
                        ,'is_dns','no_payload']#,'hours','day_of_week']
        for i in range(0,len(network_devices)):
            featureName.append("device_"+str(i))
        featureName.append("other_device")
        index = 0
        for index in range(0,10):
            estimator = forest.estimators_[index]
            export_graphviz(estimator, out_file="tree"+str(index)+".dot", class_names=["Legitimate","Illegitimate"], feature_names = featureName, impurity = False, filled = True,max_depth = 3)


def classifyOcSVM(X_train_scaled,y_train,X_test_scaled,y_test):
        global ocSVM
        # Use random forest classifier
        ocSVM = OneClassSVM()
        ocSVM.fit(X_train_scaled,y_train)

        print("Accuracy on training set: {:.3f}".format(ocSVM.score(X_train_scaled, y_train)))
        print("Accuracy on test set: {:.3f}".format(ocSVM.score(X_test_scaled, y_test)))

def printScatter(X_train_scaled,y_train):
        plotColor = []
        for i in y_train:
                if i == 'true': 
                   plotColor.append("green")
                else: 
                    plotColor.append("red")

        plt.scatter(X_train_scaled[:,0],X_train_scaled[:,1],color=plotColor)
        plt.show()




class Test(Resource):
    def post(self):
        dataset = pd.read_json(request.data,lines=True)
        #dataset['row_count'] = -1
        dataset = computeData(dataset)
        input = forest.predict(scaler.transform(dataset)).tolist()
        result = {'result': input}
        return jsonify(result)

api.add_resource(Test, '/test')     



def main():
        global attackTestValue
        global attackTestResults
        dataset = getData()
        print('getData : OK')
        target = tagData(dataset)
        print('tagData : OK')
        dataset = computeData(dataset)
        print('computeData : OK')
        print(dataset)
        #createFakeData( dataset, target)
        # Split datas
        X_train, X_test, y_train, y_test = train_test_split(dataset,target,random_state=1)
        if DEBUG:
            print(X_train.shape)
            print(X_test.shape)
        print("Before scaling: ")
        print(X_train.iloc[10:15].values)
        X_train_scaled, X_test_scaled = scaleDataset(X_train, X_test)
        print("After scaling:")
        print(X_train_scaled[10:15])
        if DEBUG:
            print(X_train_scaled)
        classify(X_train_scaled,y_train, X_test_scaled, y_test)

        # Test model with datas collected during an attack
        if DEBUG:
            attackTest = getDataWhenAttack()
            attackTestValue = attackTest
            attackTest = computeData(attackTest)
            attackTest = scaler.transform(attackTest)
            attackTestResults = forest.predict(attackTest)
            print(attackTestResults)


        #printTrees(X_train_scaled,y_train,forest)

        app.run(port='5002', host='0.0.0.0')
        
        

if __name__ == "__main__":
        main()
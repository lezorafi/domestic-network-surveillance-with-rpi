#!/bin/bash

# Restart swap 
/etc/init.d/dphys-swapfile stop
/etc/init.d/phys-swapfile start

# Install apt dependencies
# For configuring Access Point
apt-get install hostapd
# For configuring DHCP Server
apt-get install dnsmasq
# For persistent iptables configuration (Uncomment next line if you want to make iptables persistent)
#apt-get install iptables-persistent
# Used by logstash and elasticsearch
apt-get install jdk-default default-jre
# Used by logstash and elasticsearch
apt-get install jruby
# For capturing network flow
apt-get install tshark
# Used by python libraries (Useless if you don't use ML algoristhm on the pi)
#apt-get install libblas-dev liblapack-dev libatlas-base-dev gfortran


# Download logstash and elasticsearch with wget
wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-6.6.0.deb
wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-6.6.0.deb.sha512
shasum -a 512 -c elasticsearch-6.6.0.deb.sha512
if [ "$?" -ne 0 ]
then 
    echo "Elasticsearch checksum incorrect"
    exit 1
fi
dpkg -i elasticsearch-6.6.0.deb

wget https://artifacts.elastic.co/downloads/logstash/logstash-6.6.0.deb
wget https://artifacts.elastic.co/downloads/logstash/logstash-6.6.0.deb.sha512
shasum -a 512 -c logstash-6.6.0.deb.sha512
if [ "$?" -ne 0 ]
then 
    echo "Logstash checksum incorrect"
    exit 1
fi
dpkg -i logstash-6.6.0.deb

